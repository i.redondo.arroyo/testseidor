# Deploy test seidor

This is a proyect with create, update, read and delete endpoint.

## Table of Contents
- [Project Structure](#project-structure)
- [Environment Configuration](#environment-configuration)

## Project Structure

The project follows a standard structure:

- `.env`: Configuration file for environment variables.
- `api_deploys/`: Main application directory.
  - `handlers/`: HTTP request handlers.
  - `middleware/`: Middleware components.
  - `model/`: Data models.
  - `Dockerfile`: Docker configuration for the application.
  - `go.mod` and `go.sum`: Go module files.

- `sql/`: SQL scripts for database setup.
  - `migrations.sql`: Script for creating the database table.

- `docker-compose.yml`: Docker Compose configuration for running the application and its dependencies.

## Environment Configuration

The project uses a `.env` file to manage environment variables. Copy the `.env.example` file and customize it according to your needs.
