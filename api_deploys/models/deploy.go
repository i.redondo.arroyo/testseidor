package models

import (
	"database/sql"
	"encoding/json"
)

var db *sql.DB

type Deploy struct {
	DeployID int             `json:"deployId"`
	AppName  string          `json:"appName"`
	Type     string          `json:"type"`
	Version  string          `json:"version"`
	Values   json.RawMessage `json:"values"`
}

func CreateDeploy(data Deploy) error {
	query := `
		INSERT INTO deploys  (deploy_id, app_name, deploy_type , version, values)
		VALUES ($1, $2, $3, $4, $5);`
	stmt, err := db.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(
		&data.DeployID,
		&data.AppName,
		&data.Type,
		&data.Version,
		&data.Values,
	)

	return err
}

func UpdateDeploy(data Deploy) error {
	query := `
		UPDATE deploys
		SET app_name = $2,
			deploy_type = $3,
			version = $4,
			values = $5
		WHERE deploy_id = $1;`
	stmt, err := db.Prepare(query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(
		&data.DeployID,
		&data.AppName,
		&data.Type,
		&data.Version,
		&data.Values,
	)

	return err
}

func GetDeployByID(deployID int) (*Deploy, error) {
	query := `
		SELECT deploy_id, app_name, deploy_type, version, values
		FROM deploys
		WHERE deploy_id = ?;`

	var deploy Deploy
	err := db.QueryRow(query, deployID).Scan(
		&deploy.DeployID,
		&deploy.AppName,
		&deploy.Type,
		&deploy.Version,
		&deploy.Values,
	)

	if err != nil {
		return nil, err
	}

	return &deploy, nil
}

func DeleteDeploy(deployID int) error {
    query := `
        DELETE FROM deploys
        WHERE deploy_id = ?;`

    _, err := db.Exec(query, deployID)
    return err
}
