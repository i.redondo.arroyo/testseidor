package handlers

import (
	"net/http"

	"gitlab.com/i.redondo.arroyo/testseidor/models"
)

// Index endpoint
func Index(w http.ResponseWriter, r *http.Request) {
	mR := models.MyResponse{}
	mR.Code = 0
	mR.Message = "Hello World!!!!!"
	models.GenerateResponse(w, mR, http.StatusOK)
}
