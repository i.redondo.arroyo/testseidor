package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog/hlog"
	"github.com/rs/zerolog/log"
	"gitlab.com/i.redondo.arroyo/testseidor/models"
)

// CreateDeploy
func CreateDeploy(w http.ResponseWriter, r *http.Request) {
	mR := models.MyResponse{Code: 1}

	i := models.Deploy{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&i)
	if err != nil {
		mR.Message = "Missing parameters"
		hlog.FromRequest(r).Error().Caller().Err(err).Msg(mR.Message)
		models.GenerateResponse(w, mR, http.StatusBadRequest)
		return
	}
	hlog.FromRequest(r).Debug().Caller().Interface("input deploy", i).Msg("")

	err := models.CreateDeploy(i)
	if err != nil {
		log.Error().Caller().Err(err).Msg("error creating deploy")
		models.GenerateResponse(w, mR, http.StatusBadRequest)
		return
	}

	mR.Code = 0
	mR.Data = i
	mR.Message = "Deploy created"
	models.GenerateResponse(w, mR, http.StatusOK)
}

// UpdateDeploy
func CreateDeploy(w http.ResponseWriter, r *http.Request) {
	mR := models.MyResponse{Code: 1}

	i := models.Deploy{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&i)
	if err != nil {
		mR.Message = "Missing parameters"
		hlog.FromRequest(r).Error().Caller().Err(err).Msg(mR.Message)
		models.GenerateResponse(w, mR, http.StatusBadRequest)
		return
	}
	hlog.FromRequest(r).Debug().Caller().Interface("input deploy", i).Msg("")

	err := models.UpdateDeploy(i)
	if err != nil {
		log.Error().Caller().Err(err).Msg("error updating deploy")
		models.GenerateResponse(w, mR, http.StatusBadRequest)
		return
	}

	mR.Code = 0
	mR.Data = i
	mR.Message = "Deploy updated"
	models.GenerateResponse(w, mR, http.StatusOK)
}

// GetDeployByIDHandler obtiene un Deploy por deploy_id
func GetDeployByID(w http.ResponseWriter, r *http.Request) {
	mR := models.MyResponse{Code: 1}

	vars := mux.Vars(r)
	deployID, err := strconv.Atoi(vars["deploy_id"])
	if err != nil {
		mR.Message = "Invalid deploy_id"
		hlog.FromRequest(r).Error().Caller().Err(err).Msg(mR.Message)
		models.GenerateResponse(w, mR, http.StatusBadRequest)
		return
	}

	data, err := models.GetDeployByID(deployID)
	if err != nil {
		log.Error().Caller().Err(err).Msg("Error getting deploy")
		models.GenerateResponse(w, mR, http.StatusInternalServerError)
		return
	}

	mR.Code = 0
	mR.Data = data
	mR.Message = "Deploy retrieved successfully"
	models.GenerateResponse(w, mR, http.StatusOK)
}

// DeleteDeployHandler borra un deploy por deploy_id
func DeleteDeployHandler(w http.ResponseWriter, r *http.Request) {
	mR := models.MyResponse{Code: 1}

	vars := mux.Vars(r)
	deployID, err := strconv.Atoi(vars["deploy_id"])
	if err != nil {
		mR.Message = "Invalid deploy_id"
		hlog.FromRequest(r).Error().Caller().Err(err).Msg(mR.Message)
		models.GenerateResponse(w, mR, http.StatusBadRequest)
		return
	}

	err = models.DeleteDeploy(deployID)
	if err != nil {
		log.Error().Caller().Err(err).Msg("Error deleting deploy")
		models.GenerateResponse(w, mR, http.StatusInternalServerError)
		return
	}

	mR.Code = 0
	mR.Message = "Deploy deleted successfully"
	models.GenerateResponse(w, mR, http.StatusOK)
}
