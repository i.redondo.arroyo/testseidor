package main

import (
	"context"
	"flag"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/justinas/alice"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/hlog"
	"gitlab.com/i.redondo.arroyo/testseidor/handlers"
	"gitlab.com/i.redondo.arroyo/testseidor/middlewares"
	"gitlab.com/i.redondo.arroyo/testseidor/models"
)

var (
	addr = flag.String("listen-address", ":8080", "The address to listen on for HTTP requests.")

	version string
)

func init() {
	version = os.Getenv("COMMIT_ID")
}

func main() {
	flag.Parse()
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnixMicro

	// logs
	log := zerolog.New(os.Stdout).With().
		Timestamp().
		Str("version", version).
		Logger()

	ctx := context.Background()
	ctx = context.WithValue(ctx, models.IDKey{}, uuid.New())

	// graceful shutdown
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)

	// router
	r := mux.NewRouter()
	r.Use(middlewares.RecoverPanic)
	c := alice.New(hlog.NewHandler(log), hlog.AccessHandler(accessLogger))
	c = c.Append(hlog.RemoteAddrHandler("ip"))

	urlV1 := "/api/v1"

	r.HandleFunc("/index", handlers.Index).Methods("GET")
	r.HandleFunc(urlV1+"/index", handlers.Index).Methods("GET")

	r.HandleFunc(urlV1+"/deploy", middlewares.Chain(handlers.CreateDeploy)).Methods("POST")
	r.HandleFunc(urlV1+"/deploy", middlewares.Chain(handlers.UpdateDeploy)).Methods("UPDATE")
	r.HandleFunc(urlV1+"/deploy/{deploy_id}", middlewares.Chain(handlers.GetDeploy)).Methods("GET")
	r.HandleFunc(urlV1+"/deploy/{deploy_id}", middlewares.Chain(handlers.DeleteDeploy)).Methods("DELETE")

	srv := &http.Server{
		Addr:    *addr,
		Handler: c.Then(r),
	}

	go serveHTTP(ctx, log, srv)

	<-quit

	log.Info().Interface("event_id", ctx.Value(models.IDKey{})).Msg("shutdown deploy API service")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	srv.Shutdown(ctx)
}

func accessLogger(r *http.Request, status, size int, dur time.Duration) {
	hlog.FromRequest(r).Debug().
		Str("host", r.Host).
		Int("status", status).
		Str("url", r.RequestURI).
		Str("method", r.Method).
		Int("size", size).
		Dur("duration_ms", dur).
		Msg("request")
}

func serveHTTP(ctx context.Context, log zerolog.Logger, srv *http.Server) {
	log.Info().Interface("event_id", ctx.Value(models.IDKey{})).Msgf("deploy API Server started at %s", srv.Addr)
	err := srv.ListenAndServe()

	if err != http.ErrServerClosed {
		log.Error().Caller().Interface("event_id", ctx.Value(models.IDKey{})).Err(err).Msg("starting Server listener failed")
	}
}
