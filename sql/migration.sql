CREATE TABLE deploys (
    deploy_id INT PRIMARY KEY AUTO_INCREMENT,
    app_name VARCHAR(255) NOT NULL,
    deploy_type VARCHAR(255) NOT NULL,
    version VARCHAR(20) NOT NULL,
    values JSON,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
